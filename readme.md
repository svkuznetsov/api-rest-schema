# Сводная таблица
[Описание полей документов](../../raw/master/docgen/table.html)

# Описание формата json схемы
https://spacetelescope.github.io/understanding-json-schema/index.html

# Аутентификация
Осуществляется методом Basic.
Параметры шифруются алгоритмом base64 и передаются в формате *<тенант>:<логин>:<пароль>*

Например:
```
Basic dGVuYW50OnVzZXJuYW1lOnBhc3N3b3Jk
```

# Ключ API
Каждый http-запрос должен содержать http-заголовок с ключем API. Ключ API служит для однозначной идентификации клиентского приложения.

Например:
```
X-Api-Key: D28150A0-18D5-4B1B-AC4A-D514C1153544
```
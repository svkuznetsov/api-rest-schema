{
  "id": "/document/types/upd.json",
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Документ типа 'УПД'",
  "type": "object",
  "properties": {
    "id": {
      "description": "Идентификатор документа в программе получателя",
      "type": [
        "integer",
        "null"
      ]
    },
    "foreignId": {
      "description": "Идентификатор документа в клиентской программе",
      "createChange": true,
      "type": [
        "string",
        "null"
      ]
    },
    "type": {
      "description": "Тип документа: УПД",
      "type": "string",
      "enum": [
        "upd"
      ]
    },
    "date": {
      "description": "Дата документа",
      "type": "string",
      "pattern": "^\\d{4}-\\d{2}-\\d{2}$"
    },
    "number": {
      "description": "Номер документа",
      "type": "string",
      "minLength": 1
    },
    "contract": {
      "description": "Договор",
      "type": [
        "object",
        "null"
      ],
      "properties": {
        "number": {
          "description": "Номер договора",
          "type": "string"
        },
        "date": {
          "type": "string",
          "description": "Дата договора",
          "pattern": "^\\d{4}-\\d{2}-\\d{2}$"
        },
        "fromContractor": {
          "description": "Поставщик",
          "type": "object",
          "properties": {
            "id": {
              "description": "Идентификатор контрагента",
              "type": "integer"
            },
            "name": {
              "description": "Наименование",
              "type": "string",
              "minLength": 1
            },
            "inn": {
              "description": "ИНН",
              "type": "string"
            },
            "kpp": {
              "description": "КПП",
              "type": [
                "string",
                "null"
              ]
            }
          }
        },
        "toContractor": {
          "description": "Покупатель",
          "type": "object",
          "properties": {
            "id": {
              "description": "Идентификатор контрагента",
              "type": "integer"
            },
            "name": {
              "description": "Наименование",
              "type": "string",
              "minLength": 1
            },
            "inn": {
              "description": "ИНН",
              "type": "string"
            },
            "kpp": {
              "description": "КПП",
              "type": [
                "string",
                "null"
              ]
            }
          }
        }
      }
    },
    "sum": {
      "description": "Общая сумма по документу без НДС",
      "type": "number",
      "minimum": 0.0,
      "default": 0.0
    },
    "vatSum": {
      "description": "Суммарный НДС по документу",
      "type": "number",
      "minimum": 0.0,
      "default": 0.0
    },
    "sumWithVat": {
      "description": "Общая сумма по документу вместе с НДС",
      "type": "number",
      "minimum": 0.0,
      "default": 0.0
    },
    "prepayment": {
      "description": "Аванс (предоплата)",
      "type": "boolean",
      "default": false
    },
    "basedOn": {
      "description": "Основание передачи/получения/приёмки",
      "type": [
        "string",
        "null"
      ]
    },
    "shipmentDate": {
      "description": "Дата отгрузки / передачи",
      "type": "string",
      "pattern": "^\\d{4}-\\d{2}-\\d{2}$"
    },
    "receiptDate": {
      "description": "Дата получения / приемки",
      "type": "string",
      "pattern": "^\\d{4}-\\d{2}-\\d{2}$"
    },
    "transferDocumentStatus": {
      "description": "Статус передаточного документа. 1 - счет-фактура и передаточный документ, 2 - передаточный документ.",
      "type": "integer",
      "enum": [
        1,
        2
      ]
    },
    "transferDocumentType": {
      "description": "Вид передаточного документа. act - акт, wbll - товарная накладная",
      "type": "string",
      "enum": [
        "act",
        "wbll"
      ]
    },
    "transportationInfo": {
      "description": "Данные о транспортировке и грузе",
      "type": [
        "string",
        "null"
      ]
    },
    "receiptInfo": {
      "description": "Иные сведения о приёмке / получении",
      "type": [
        "string",
        "null"
      ]
    },
    "otherDataAboutTransmission": {
      "description": "Иные сведения о передаче / отгрузке",
      "type": [
        "string",
        "null"
      ]
    },
    "fromContractor": {
      "description": "Поставщик",
      "type": "object",
      "properties": {
        "inn": {
          "description": "ИНН",
          "type": "string"
        },
        "name": {
          "description": "Наименование",
          "type": "string",
          "minLength": 1
        },
        "kpp": {
          "description": "КПП",
          "type": [
            "string",
            "null"
          ]
        },
        "registered": {
          "description": "Дата регистрации",
          "type": [
            "string",
            "null"
          ],
          "pattern": "^\\d{4}-\\d{2}-\\d{2}$"
        },
        "taxSystemType": {
          "description": "Тип системы налогообложения. COMMON - ОСН, SIMPLIFIED_TURNOVER - УСН Доходы, SIMPLIFIED_INCOME_MINUS_COSTS - УСН Доходы минус расходы",
          "type": [
            "string",
            "null"
          ],
          "enum": [
            "COMMON",
            "SIMPLIFIED_TURNOVER",
            "SIMPLIFIED_INCOME_MINUS_COSTS"
          ]
        }
      }
    },
    "fromContractorAddress": {
      "description": "Адрес поставщика",
      "type": [
        "object",
        "null"
      ],
      "properties": {
        "fullAddress": {
          "description": "Строка адреса",
          "type": [
            "string",
            "null"
          ],
          "default": null
        },
        "type": {
          "description": "Физический или юридический",
          "type": [
            "string",
            "null"
          ],
          "enum": [
            "PHYSICAL",
            "LEGAL"
          ],
          "default": "LEGAL"
        }
      }
    },
    "fromShipperIsFromContractor": {
      "description": "Продавец и грузоотправитель являются одним и тем же лицом. При значении true значения полей fromShipper* будут приняты равными значениям соответствующих полей fromContractor*.",
      "type": "boolean",
      "default": true
    },
    "fromShipper": {
      "description": "Грузоотправитель",
      "type": [
        "object",
        "null"
      ],
      "properties": {
        "inn": {
          "description": "ИНН",
          "type": "string"
        },
        "name": {
          "description": "Наименование",
          "type": "string",
          "minLength": 1
        },
        "kpp": {
          "description": "КПП",
          "type": [
            "string",
            "null"
          ]
        }
      }
    },
    "fromShipperAddress": {
      "description": "Адрес грузоотправителя",
      "type": [
        "object",
        "null"
      ],
      "properties": {
        "fullAddress": {
          "description": "Строка адреса",
          "type": [
            "string",
            "null"
          ],
          "default": null
        },
        "type": {
          "description": "Физический или юридический",
          "type": [
            "string",
            "null"
          ],
          "enum": [
            "PHYSICAL",
            "LEGAL"
          ],
          "default": "LEGAL"
        }
      }
    },
    "toContractor": {
      "description": "Покупатель",
      "type": "object",
      "properties": {
        "inn": {
          "description": "ИНН",
          "type": "string"
        },
        "name": {
          "description": "Наименование",
          "type": "string",
          "minLength": 1
        },
        "kpp": {
          "description": "КПП",
          "type": [
            "string",
            "null"
          ]
        },
        "registered": {
          "description": "Дата регистрации",
          "type": [
            "string",
            "null"
          ],
          "pattern": "^\\d{4}-\\d{2}-\\d{2}$"
        },
        "taxSystemType": {
          "description": "Тип системы налогообложения. COMMON - ОСН, SIMPLIFIED_TURNOVER - УСН Доходы, SIMPLIFIED_INCOME_MINUS_COSTS - УСН Доходы минус расходы",
          "type": [
            "string",
            "null"
          ],
          "enum": [
            "COMMON",
            "SIMPLIFIED_TURNOVER",
            "SIMPLIFIED_INCOME_MINUS_COSTS"
          ]
        }
      }
    },
    "toContractorAddress": {
      "description": "Адрес покупателя",
      "type": [
        "object",
        "null"
      ],
      "properties": {
        "fullAddress": {
          "description": "Строка адреса",
          "type": [
            "string",
            "null"
          ],
          "default": null
        },
        "type": {
          "description": "Физический или юридический",
          "type": [
            "string",
            "null"
          ],
          "enum": [
            "PHYSICAL",
            "LEGAL"
          ],
          "default": "LEGAL"
        }
      }
    },
    "toShipperIsToContractor": {
      "description": "Покупатель и грузополучатель являются одним и тем же лицом. При значении true значения полей toShipper* будут приняты равными значениям соответствующих полей toContractor*.",
      "type": "boolean",
      "default": true
    },
    "toShipper": {
      "description": "Грузополучатель",
      "type": [
        "object",
        "null"
      ],
      "properties": {
        "inn": {
          "description": "ИНН",
          "type": "string"
        },
        "name": {
          "description": "Наименование",
          "type": "string",
          "minLength": 1
        },
        "kpp": {
          "description": "КПП",
          "type": [
            "string",
            "null"
          ]
        }
      }
    },
    "toShipperAddress": {
      "description": "Адрес грузополучателя",
      "type": [
        "object",
        "null"
      ],
      "properties": {
        "fullAddress": {
          "description": "Строка адреса",
          "type": [
            "string",
            "null"
          ],
          "default": null
        },
        "type": {
          "description": "Физический или юридический",
          "type": [
            "string",
            "null"
          ],
          "enum": [
            "PHYSICAL",
            "LEGAL"
          ],
          "default": "LEGAL"
        }
      }
    },
    "positions": {
      "description": "Позиции. Табличная часть документа.",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "nomenclature": {
            "description": "Наименование товара",
            "type": "object",
            "properties": {
              "name": {
                "type": [
                  "string",
                  "null"
                ]
              }
            }
          },
          "productCode": {
            "description": "Код товара/работ, услуг",
            "type": [
              "string",
              "null"
            ]
          },
          "amount": {
            "description": "Количество товара",
            "type": "number",
            "minimum": 0,
            "default": 0
          },
          "unit": {
            "description": "Единица измерения",
            "type": "object",
            "properties": {
              "name": {
                "description": "Наименование единицы измерения",
                "type": [
                  "string",
                  "null"
                ]
              },
              "code": {
                "description": "Краткое наименование единицы измерения",
                "type": [
                  "string",
                  "null"
                ]
              }
            }
          },
          "price": {
            "description": "Цена за единицу",
            "type": "number",
            "minimum": 0,
            "default": 0
          },
          "countryName": {
            "description": "Краткое наименование страны",
            "type": [
              "string",
              "null"
            ]
          },
          "countryCode": {
            "description": "Код страны",
            "type": [
              "string",
              "null"
            ]
          },
          "declaration": {
            "description": "Номер таможенной декларации",
            "type": [
              "string",
              "null"
            ]
          },
          "vat": {
            "description": "Ставка налогообложения",
            "type": "object",
            "properties": {
              "rate": {
                "description": "Показатель ставки (например, 18)",
                "type": [
                  "integer",
                  "null"
                ],
                "minimum": 0,
                "maximum": 100
              },
              "name": {
                "description": "Наименование (например, 18%)",
                "type": [
                  "string",
                  "null"
                ]
              }
            }
          },
          "sum": {
            "description": "Сумма без НДС",
            "type": "number",
            "minimum": 0.0,
            "default": 0.0
          },
          "vatSum": {
            "description": "Сумма НДС (сумма налога)",
            "type": "number",
            "minimum": 0.0,
            "default": 0.0
          },
          "sumWithVat": {
            "description": "Сумма вместе с НДС",
            "type": "number",
            "minimum": 0.0,
            "default": 0.0
          },
          "exciseSum": {
            "description": "Сумма акциза (уже входит в сумму без НДС)",
            "type": "number",
            "minimum": 0.0,
            "default": 0.0
          }
        }
      }
    }
  },
  "required": [
    "type",
    "date",
    "number",
    "fromContractor",
    "toContractor",
    "sum",
    "vatSum",
    "sumWithVat",
    "prepayment",
    "positions",
    "transferDocumentStatus",
    "transferDocumentType"
  ]
}
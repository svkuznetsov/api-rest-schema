#!/usr/bin/env bash

# отменить событие (отметить как невостребованное); вернет запись, получившуюся после обновления статуса

source ../../config.sh

curl -i \
    --max-time 300 \
    --trace-ascii /dev/stdout \
    --user ${CREDENTIALS} \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --header "X-Api-Key: ${APIKEY}" \
    --data "@markCancelled.json" \
    -X PUT "{$URL}/api/rest/exchange/operation/1000000"
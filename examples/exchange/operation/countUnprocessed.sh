#!/usr/bin/env bash

# подсчет необработанных исходящих событий изменения документа

source ../../config.sh

curl -i \
    --max-time 300 \
    --trace-ascii /dev/stdout \
    --user ${CREDENTIALS} \
    --header "Content-Type: application/json" \
    --header "X-Api-Key: ${APIKEY}" \
    --data "@countUnprocessed.json" \
    -X POST "{$URL}/api/rest/exchange/operation/count"
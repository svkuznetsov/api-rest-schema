#!/usr/bin/env bash

# скачивание файла дистрибутива обработчика 1С

source ../../../../config.sh

curl -i \
    --max-time 300 \
    --trace-ascii /dev/stdout \
    --trace-time \
    --request GET "${URL}/api/rest/settings/1c/plugin/distro/default"
#!/usr/bin/env bash

# получение мета-данных о файле дистрибутива обработчика 1С

source ../../../../config.sh

curl -i \
    --max-time 300 \
    --trace-ascii /dev/stdout \
    --trace-time \
    --head "${URL}/api/rest/settings/1c/plugin/distro/default"
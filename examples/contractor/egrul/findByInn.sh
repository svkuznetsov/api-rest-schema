#!/usr/bin/env bash

# получение информации о ЮЛ по ИНН из ЕГРЮЛ

source ../../config.sh

INN="${1}"

if [ -z "${INN}" ] ; then
  INN="7805187876"
fi

curl -i \
    --max-time 300 \
    --trace-ascii /dev/stdout \
    --user "${CREDENTIALS}" \
    -X GET "${URL}/api/rest/contractor/egrul/${INN}"
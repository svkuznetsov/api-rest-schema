#!/usr/bin/env bash

# добавление счета-фактуры

source ../../config.sh

NUMBER=`date '+%F\/%H:%M:%S\/%N'`
DATE=`date '+%F'`

cat invoice.json | \
sed -e "s/\$NUMBER/${NUMBER}/g" | \
sed -e "s/\$DATE/${DATE}/g" | \
curl -i \
    --max-time 300 \
    --trace-ascii /dev/stdout \
    --user ${CREDENTIALS} \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --header "X-Api-Key: ${APIKEY}" \
    --data "@-" \
    -X PUT "{$URL}/api/rest/document"
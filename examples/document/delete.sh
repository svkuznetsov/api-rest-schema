#!/usr/bin/env bash

# удаление документа по ID

source ../config.sh

ID="${1}"

if [ -z "${ID}" ] ; then
  ID="37769947"
fi

curl -i \
    --max-time 300 \
    --trace-ascii /dev/stdout \
    --user "${CREDENTIALS}" \
    --header "X-Api-Key: ${APIKEY}" \
    -X DELETE "${URL}/api/rest/document/${ID}"
#!/usr/bin/env bash

# вычитка нескольких документов сразу

source ../../config.sh

curl -i \
    --max-time 300 \
    --trace-ascii /dev/stdout \
    --user ${CREDENTIALS} \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --header "X-Api-Key: ${APIKEY}" \
    --data "@findByIdBatch.json" \
    -X POST "{$URL}/api/rest/document/batch"
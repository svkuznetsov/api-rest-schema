#!/usr/bin/env bash

# поиск по индексу документов

source ../../config.sh

curl -i \
    --max-time 300 \
    --trace-ascii /dev/stdout \
    --user ${CREDENTIALS} \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --header "X-Api-Key: ${APIKEY}" \
    --data "@index.json" \
    -X POST "{$URL}/api/rest/document/index"
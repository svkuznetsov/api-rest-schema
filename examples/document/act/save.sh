#!/usr/bin/env bash

# добавление акта выполненных работ и оказанных услуг

source ../../config.sh

curl -i \
    --max-time 300 \
    --trace-ascii /dev/stdout \
    --user ${CREDENTIALS} \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --header "X-Api-Key: ${APIKEY}" \
    --data "@act.json" \
    -X PUT "{$URL}/api/rest/document"
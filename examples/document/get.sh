#!/usr/bin/env bash

# получение документа по ID

source ../config.sh

ID="${1}"

if [ -z "${ID}" ] ; then
  ID=""
fi

curl -i \
    --max-time 300 \
    --trace-ascii /dev/stdout \
    --user "${CREDENTIALS}" \
    --header "X-Api-Key: ${APIKEY}" \
    -X GET "${URL}/api/rest/document/${ID}"
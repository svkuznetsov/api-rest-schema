var Paths = Java.type('java.nio.file.Paths');
var Files = Java.type('java.nio.file.Files');
var StandardCharsets = Java.type('java.nio.charset.StandardCharsets');
var BufferedWriter = Java.type('java.io.BufferedWriter');
var FileWriter = Java.type('java.io.FileWriter');
var File = Java.type('java.io.File');

var schemaDirectory = '../schema/document/types';
var schemaFiles = [
    'act.json',
    'bill.json',
    'cash_pay_voucher.json',
    'cash_rec_voucher.json',
    'contract.json',
    'invoice.json',
    'paymentOrder.json',
    'upd.json',
    'waybill.json',
    'payroll.json',
    'receipt.json'
];

var schemas = readSchemas(schemaDirectory, schemaFiles);
var properties = invert(schemas);

var tags = {
    'date':
        new Date().toLocaleString(),
    'style':
        [fileText, Paths.get('./assets/style.css')],
    'script':
        [fileText, Paths.get('./assets/script.js')],
    'types-selector':
        [renderTypesSelector, schemas],
    'search-suggests':
        [renderSuggests, properties],
    'table':
        [renderTable, schemas, properties]
};

var writer;
try {
    writer = new BufferedWriter(new FileWriter(new File("./table.html"), false));
    renderTemplate(fileText(Paths.get('./assets/layout.html')), tags);
} finally {
    writer && writer.close();
}

function out() {
    for (var i = 0; i < arguments.length; i++) {
        var chunk = arguments[i];
        if (chunk !== undefined) {
            if (Array.isArray(chunk)) {
                if (typeof chunk[0] == 'function') {
                    out(chunk[0].apply(null, chunk.slice(1)));
                } else {
                    out.apply(null, chunk);
                }
            } else if (typeof chunk == 'function') {
                out(chunk.apply(null));
            } else {
                writer.write(String(chunk));
            }
        }
    }
}

function escape(value) {
    if (!value || !(typeof value.replace == 'function')) {
        return value;
    }
    return value.replace(/[\u00A0-\u9999<>\&]/gim, function(i) {
        return '&#' + i.charCodeAt(0) + ';';
    });
}

function renderTemplate(tpl, tags) {
    var pos = 0;
    Object.keys(tags)
    .reduce(function(accumulator, tag) {
        var placeholder = '${' + tag + '}';
        var index = -1;
        do {
            index = tpl.indexOf(placeholder, index + 1);
            if (index > -1) {
                accumulator.push([index, placeholder, tags[tag]]);
            }
        } while (index > -1);
        return accumulator;
    }, [])
    .sort(function(a, b) {
        return a[0] - b[0];
    })
    .forEach(function(replacement) {
        var chunk = tpl.substring(pos, replacement[0]);
        out(chunk);
        pos = replacement[0] + replacement[1].length;
        out(replacement[2]);
    });
    out(tpl.substring(pos, tpl.length));
}

function schemaName(file) {
    return file.split('.').slice(0, -1).join('.');
}

function fileText(path) {
    return Java.from(Files.readAllLines(path, StandardCharsets.UTF_8)).join("\n");
}

function assoc(object, property, value) {
    var result = object || {};
    result[property] = value;
    return result;
}

function aggregate(object, callback, accumulator) {
    if (Array.isArray(object)) {
        return object.reduce(function (accumulator, value) {
            return callback(accumulator, value);
        }, accumulator || {});
    } else if (object) {
        return Object.keys(object).reduce(function (accumulator, key) {
            return callback(accumulator, key, object[key]);
        }, accumulator || {});
    }
    return object;
}

function readSchemas(directory, files) {
    return aggregate(files, function (accumulator, file) {
        return assoc(accumulator, schemaName(file), JSON.parse(fileText(Paths.get(directory, file))));
    });
}

function invert(schemas) {
    return aggregate(schemas, function (accumulator, name, properties) {
        return aggregate(flatten(properties), function (accumulator, property, value) {
            return assoc(accumulator, property, assoc(accumulator[property], name, value));
        }, accumulator);
    });
}

function flatten(schema, level) {
    if (!level) {
        level = 0;
    }
    return aggregate(schema.properties, function (accumulator, key, value) {
        var item = {};
        var subItems = [];

        if (value.properties) {
            subItems.push(flatten(value, level + 1));
        }
        if (value.patternProperties) {
            subItems.push(flatten({properties: value.patternProperties}, level + 1));
        }
        if (value.items) {
            if (Array.isArray(value.items)) {
                if (value.items[0]) {
                    subItems.push(flatten(value.items[0], level + 1));
                }
            } else {
                subItems.push(flatten(value.items, level + 1));
            }
        }

        if (value.type) {
            if (Array.isArray(value.type)) {
                var nullIndex = value.type.indexOf("null");
                item.nullable = nullIndex > -1;
                if (item.nullable) {
                    value.type.splice(nullIndex, 1);
                }
                item.type = value.type.sort().join(', ');
            } else {
                item.type = value.type;
                item.nullable = item.type == "null";
                if (item.nullable) {
                    item.type = null;
                }
            }
        }

        if (value.enum && value.enum.join) {
            item.enum = value.enum.sort();
        }

        if (value.pattern) {
            item.pattern = value.pattern;
        }

        if (value.minLength !== undefined) {
            item.minLength = value.minLength;
        }

        if (value.maxLength !== undefined) {
            item.maxLength = value.maxLength;
        }

        if (value.minimum !== undefined) {
            item.minimum = value.minimum;
        }

        if (value.maximum !== undefined) {
            item.maximum = value.maximum;
        }

        if (value.description) {
            item.description = value.description;
        }

        if (value.modifiedSkip) {
            item.modifiedSkip = true;
        }

        if (value.createChange) {
            item.createChange = true;
        }

        if (value.default) {
            item.default = value.default;
        }

        if (level == 0 && schema.required && schema.required.indexOf) {
            if (schema.required.indexOf(key) > -1) {
                item.required = true;
            }
        }

        aggregate(subItems, function (accumulator, subItem) {
            return aggregate(subItem, function (accumulator, subKey, value) {
                return assoc(accumulator, key + '.' + subKey, value);
            }, accumulator);
        }, accumulator);

        return assoc(accumulator, key, item);
    });
}

function renderTypesSelector(schemas) {
    Object.keys(schemas).forEach(function(value) {
        out(
            '<div>',
                '<label>',
                    '<input type="checkbox" checked="checked" value="', value,'"/>', value,
                '</label>',
            '</div>'
        );
    });
}

function renderSuggests(properties) {
    var descriptions = aggregate(properties, function(accumulator, key, value) {
        return aggregate(value, function(accumulator, key, value) {
            return value.description ? assoc(accumulator, value.description, true) : accumulator;
        }, accumulator);
    });
    Object.keys(properties).concat(Object.keys(descriptions)).sort().forEach(function(value) {
        out(
            '<option value="', escape(value),'"/>'
        );
    });
}

function renderTable(schemas, properties) {
    var propertyNames = Object.keys(properties).sort();
    var schemaNames = Object.keys(schemas).sort();

    var descriptions = propertyNames.reduce(function (accumulator, propertyName) {
        var property = properties[propertyName];
        return schemaNames.reduce(function (accumulator, schemaName) {
            if (property[schemaName]) {
                var description = property[schemaName].description;
                if (description) {
                    if (!accumulator[propertyName]) {
                        accumulator[propertyName] = [];
                    }
                    if (accumulator[propertyName].indexOf(description) < 0) {
                        accumulator[propertyName].push(description);
                    }
                }
            }
            return accumulator;
        }, accumulator);
    }, {});

    var repeatCol = 8;
    var repeatRow = 16;

    function renderHeader() {
        for (var i = 0; i < schemaNames.length; i++) {
            if (i % repeatCol == 0) {
                out('<th>&nbsp;</th>');
            }
            out('<th id="', schemaNames[i],'" class="header">', schemaNames[i], '</th>');
        }
    }

    function renderColumns() {
        for (var i = 0; i < schemaNames.length; i++) {
            if (i % repeatCol == 0) {
                out('<col/>');
            }
            out('<col class="value"/>');
        }
    }

    function renderProperties(property) {
        if (property.type) {
            out('<span class="tag type" title="type">');
            out(property.type);
            if (property.enum && property.enum.length > 0) {
                out(' [ <span class="constraint">', property.enum.join('</span> | <span class="constraint">'), '</span> ]');
            }
            if (property.pattern) {
                out(' [ <span class="constraint">', property.pattern, '</span> ]');
            }
            if (property.minLength != undefined) {
                out(' [ <span class="constraint">', 'length &ge; ', property.minLength, '</span> ]');
            }
            if (property.maxLength != undefined) {
                out(' [ <span class="constraint">', 'length &le; ', property.maxLength, '</span> ]');
            }
            if (property.minimum !== undefined) {
                out(' [ <span class="constraint">', '&ge; ', property.minimum, '</span> ]');
            }
            if (property.maximum != undefined) {
                out(' [ <span class="constraint">', '&le; ', property.maximum, '</span> ]');
            }
            out('</span>');
        }

        if (property.nullable) {
            out('<span class="tag nullable" title="nullable">null</span>');
        }

        if (property.required) {
            out('<span class="tag required" title="required">required</span>');
        }

        if (property.modifiedSkip) {
            out('<span class="tag ignored" title="ignored">ignored</span>');
        }

        if (property.createChange) {
            out('<span class="tag change" title="change">change</span>');
        }

        if (property.default !== undefined) {
            out('<span class="tag default" title="default">', escape(property.default), '</span>');
        }
    }

    out(
        '<colgroup>', renderColumns, '</colgroup>',
        '<thead>',
            '<tr>', renderHeader, '</tr>',
        '</thead>',
        '<tbody>',
        //renderColumns,
        function() {
            propertyNames.forEach(function (propertyName, i) {
                if ((i + 1) % repeatRow == 0) {
                    out('<tr>', renderHeader, '</tr>');
                }

                var property = properties[propertyName];
                out('<tr id="', propertyName ,'">');
                schemaNames.forEach(function (schemaName, i) {
                    if (i % repeatCol == 0) {
                        out(
                            '<th>',
                                propertyName.split('.').join('<span class="sep">.</span>'),
                                '<div class="description">',
                                    descriptions[propertyName] ? escape(descriptions[propertyName].join(' / ')) : '&nbsp;',
                                '</div>',
                            '</th>'
                        );
                    }
                    var value = property ? property[schemaName] : null;
                    if (!value) {
                        out('<td class="empty">&nbsp;</td>');
                    } else {
                        out('<td>', [renderProperties, value], '</td>');
                    }
                });
                out('</tr>');
            });
        },
        '</tbody>'
    );
}
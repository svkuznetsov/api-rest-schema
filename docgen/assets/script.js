document.addEventListener('DOMContentLoaded', function () {
    initialize();
});

function initialize() {
    var search = document.getElementById('search');
    var types = document.getElementById('types-selector');
    var properties = document.getElementById('properties');

    wireTypes(types, properties);
    wireSearch(search, properties);
}

function wireTypes(types, properties) {
    types.addEventListener('change', function(event) {
        var target = event.target;
        filterColumns(types, properties, target.value, target.checked);
    }, true);
}

function filterColumns(types, properties, value, checked) {
    var i;

    if (value == 'all') {
        var checkboxes = types.getElementsByTagName('input');
        for (i = 0; i < checkboxes.length; i++) {
            var checkbox = checkboxes[i];
            if (checkbox.value == 'all') {
                continue;
            }
            checkbox.checked = checked;
            filterColumns(types, properties, checkbox.value, checkbox.checked);
        }
    }

    var rows = properties.getElementsByTagName('tr');

    var header = rows[0];

    var headerCells = header.getElementsByTagName('th');

    var index = null;
    for (i = 0; i < headerCells.length; i++) {
        if (headerCells[i].id == value) {
            index = i;
            break;
        }
    }

    if (index === null) {
        return;
    }

    var col = properties.getElementsByTagName('col')[index];
    col.style.visibility = checked ? '' : 'collapse';
    col.style.display = checked ? '' : 'none';

    for (i = 0; i < rows.length; i++) {
        var row = rows[i];
        var cell = row.children[index];
        cell.style.visibility = checked ? '' : 'collapse';
        cell.style.display = checked ? '' : 'none';
    }
}

function wireSearch(search, properties) {
    var timeout;
    search.addEventListener('change', function() {
        if (timeout) {
            window.clearTimeout(timeout);
            timeout = null;
        }
        var term = search.value;
        timeout = window.setTimeout(function() {
            if (timeout) {
                window.clearTimeout(timeout);
                timeout = null;
            }
            filterRows(properties, term);
        }, 500);
    });
}

function filterRows(properties, term) {
    var rows = properties.getElementsByTagName('tr');

    var i;

    if (!term) {
        for (i = 0; i < rows.length; i++) {
            rows[i].style.display = 'table-row';
        }
        return;
    }

    var pattern = new RegExp(term, 'i');
    for (i = 0; i < rows.length; i++) {
        var row = rows[i];
        var matches = false;

        if (i == 0) {
            matches = true;
        } else if (row.id) {
            matches = pattern.test(row.id);
            if (!matches) {
                var th = row.getElementsByTagName('th');
                if (th) {
                    th = th[0];
                }
                if (th) {
                    var description = row.getElementsByClassName('description');
                    if (description) {
                        description = description[0].innerHTML;
                    }
                    if (description) {
                        matches = pattern.test(description);
                    }
                }
            }
        }

        row.style.display = matches ? 'table-row' : 'none';
    }
}